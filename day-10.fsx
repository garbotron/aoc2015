let lookAndSay nums = seq {
  let mutable count = 0
  let mutable cur = -1
  for num in nums do
    if num = cur then
      count <- count + 1
    else
      if count <> 0 then
        yield! [count; cur]
      count <- 1
      cur <- num
  yield! [count; cur]
}

let repeat fn n = List.init n (fun _ -> fn) |> List.reduce (>>)

printfn "Part 1: %d" (seq {1;1;1;3;2;2;2;1;1;3} |> repeat lookAndSay 40 |> Seq.length)
printfn "Part 2: %d" (seq {1;1;1;3;2;2;2;1;1;3} |> repeat lookAndSay 50 |> Seq.length)

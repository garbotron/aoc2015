let input = System.IO.File.ReadAllText("day-03-input.txt").Trim()

let visit (x, y) = function
  | '<' -> (x - 1, y)
  | '>' -> (x + 1, y)
  | '^' -> (x, y - 1)
  | 'v' -> (x, y + 1)
  | _ -> failwith "bad dir"

printfn "Part 1: %d" (input |> Seq.scan visit (0, 0) |> Seq.distinct |> Seq.length)

let santa = input |> Seq.chunkBySize 2 |> Seq.map (Array.item 0)
let robo = input |> Seq.chunkBySize 2 |> Seq.choose (Array.tryItem 1)

Seq.concat [santa |> Seq.scan visit (0, 0); robo |> Seq.scan visit (0, 0)]
  |> Seq.distinct
  |> Seq.length
  |> printfn "Part 2: %d"

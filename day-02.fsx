let input =
  System.IO.File.ReadAllLines "day-02-input.txt"
  |> Seq.map (fun x -> x.Split 'x')
  |> Seq.map (fun x -> int x.[0], int x.[1], int x.[2])

let volume (l, w, h) = l * w * h
let surfaceArea (l, w, h) = (2 * l * w) + (2 * l * h) + (2 * w * h)
let smallestPerimeter (l, w, h) = List.min [2 * (l + w); 2 * (l + h); 2 * (w + h)]
let wrapping (l, w, h) = surfaceArea (l, w, h) + List.min [l * w; l * h; w * h]
let ribbon x = smallestPerimeter x + volume x

printfn "Part 1: %d" (input |> Seq.sumBy wrapping)
printfn "Part 2: %d" (input |> Seq.sumBy ribbon)

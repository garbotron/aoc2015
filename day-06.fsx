open System.Text.RegularExpressions

// My functional solution was really slow, so we'll do it iteratively.
let foldP1 (lights: bool[,]) (str: string) =
  let m = Regex.Match(str, "(turn on|turn off|toggle) ([0-9]+),([0-9]+) through ([0-9]+),([0-9]+)")
  let x1 = m.Groups.[2].Value |> int
  let y1 = m.Groups.[3].Value |> int
  let x2 = m.Groups.[4].Value |> int
  let y2 = m.Groups.[5].Value |> int
  let op =
    match m.Groups.[1].Value with
    | "toggle" -> not
    | "turn on" -> fun _ -> true
    | "turn off" -> fun _ -> false
    | _ -> failwith "bad instruction"
  for y in {y1..y2} do
    for x in {x1..x2} do
      lights.[x, y] <- op lights.[x, y]
  lights

let foldP2 (lights: int[,]) (str: string) =
  let m = Regex.Match(str, "(turn on|turn off|toggle) ([0-9]+),([0-9]+) through ([0-9]+),([0-9]+)")
  let x1 = m.Groups.[2].Value |> int
  let y1 = m.Groups.[3].Value |> int
  let x2 = m.Groups.[4].Value |> int
  let y2 = m.Groups.[5].Value |> int
  let op =
    match m.Groups.[1].Value with
    | "toggle" -> (+) 2
    | "turn on" -> (+) 1
    | "turn off" -> fun x -> max 0 (x - 1)
    | _ -> failwith "bad instruction"
  for y in {y1..y2} do
    for x in {x1..x2} do
      lights.[x, y] <- op lights.[x, y]
  lights

let input = System.IO.File.ReadAllLines "day-06-input.txt"

let allOff = Array2D.create 1000 1000 false
printfn "Part 1: %d" (input |> Seq.fold foldP1 allOff |> Seq.cast<bool> |> Seq.filter id |> Seq.length)

let allZero = Array2D.create 1000 1000 0
printfn "Part 2: %d" (input |> Seq.fold foldP2 allZero |> Seq.cast<int> |> Seq.sum)

let nice1 (str: string) =
  str |> Seq.filter (fun x -> ['a'; 'e'; 'i'; 'o'; 'u'] |> List.contains x) |> Seq.length >= 3
  && str |> Seq.pairwise |> Seq.exists (fun (x, y) -> x = y)
  && not (["ab"; "cd"; "pq"; "xy"] |> List.exists str.Contains)

let rec hasRepeatingPair (str: string) =
  if str.Length < 4 then
    false
  elif str.Substring(2).Contains(str.Substring(0, 2)) then
    true
  else
    hasRepeatingPair (str.Substring(1))

let nice2 (str: string) =
  hasRepeatingPair str
  && (str |> Seq.toList |> List.windowed 3 |> List.exists (fun x -> x.[0] = x.[2]))

let input = System.IO.File.ReadAllLines "day-05-input.txt"
printfn "Part 1: %d" (input |> Seq.filter nice1 |> Seq.length)
printfn "Part 2: %d" (input |> Seq.filter nice2 |> Seq.length)

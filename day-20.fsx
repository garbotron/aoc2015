// ----- Part 1 -----
// Quick factors of an integer algorithm from: https://www.rosettacode.org/wiki/Factors_of_an_integer#F.23
let factors number = seq {
  for divisor in 1 .. (number |> float |> sqrt |> int) do
  if number % divisor = 0 then
    yield divisor
    yield number / divisor
}
let presents house = house |> factors |> Seq.sumBy ((*) 10)
printfn "Part 1: %d" (Seq.initInfinite id |> Seq.find (fun x -> presents x >= 34000000))

// ----- Part 2 -----
let factors' number = seq {
  for divisor in 1 .. (number |> float |> sqrt |> int) do
  if number % divisor = 0 then
    if divisor < 50 then yield number / divisor
    if number / divisor < 50 then yield divisor
}
let presents' house = house |> factors' |> Seq.sumBy ((*) 11)
printfn "Part 2: %d" (Seq.initInfinite id |> Seq.find (fun x -> presents' x >= 34000000))

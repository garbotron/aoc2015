// Change a molecule string to a list of element strings.
let rec tokenize idx (molecule: string) =
  if idx >= molecule.Length then
    []
  elif idx < molecule.Length - 1 && molecule.[idx + 1] |> System.Char.IsLower then
    molecule.Substring(idx, 2) :: tokenize (idx + 2) molecule
  else
    molecule.Substring(idx, 1) :: tokenize (idx + 1) molecule

let (initMolecule, replacements) =
  let foldReplacement map (str: string) =
    let split = str.Split " => "
    let cur = map |> Map.tryFind split.[0] |> Option.defaultValue []
    map |> Map.add split.[0] ((split.[1] |> tokenize 0) :: cur)
  let split = System.IO.File.ReadAllText("day-19-input.txt").Trim().Split("\n\n")
  (split.[1] |> tokenize 0), (split.[0].Split("\n") |> Seq.fold foldReplacement Map.empty)

let rec mutations = function
  | [] -> Seq.empty
  | head :: tail ->
    replacements
    |> Map.tryFind head
    |> Option.defaultValue []
    |> Seq.map (fun x -> x @ tail)
    |> Seq.append (tail |> mutations |> Seq.map (fun x -> head :: x))

printfn "Part 1: %d" (initMolecule |> mutations |> Seq.distinct |> Seq.length)

// Part 2 is... a mess. I couldn't think of a good general algorithm for it.
// I think that you generally want to move backward. One step at a time, un-replace a molecule until you eventually
// wind up with e. To facilitate this, I created a custom data structure to match on reverse mappings one element at a
// time. However this approach was still too broad - there are too many backward replacements.
//
// I was able to get it to run acceptably fast by exploiting a pattern in our input set. Specifically:
// - "Rn" and "Ar" cannot react - they are only the product of reactions.
// - In these reactions, both "Rn" and "Ar" are always present, in that order.
// - All of these reactions END with "Ar".
//
// Because of this, I was able to break up the input into multiple sub-sequences, each ending with "Ar". Then, starting
// with the first sub-sequence, I un-react each sequence into a single element (since it is guaranteed to be an
// independent reaction). Then, using this element, I move forward to the next sub-sequence, and so on, until the
// final sub-sequence (plus the first letter) will produce "e".
//
// I KNOW this isn't the answer AoC wanted, but I've spent hours on this and it works, so I'll take it.

type RevMap = {
  Elements: string list // elements that produce this exact sequence
  Continuations: Map<string, RevMap> // for longer sequences that start with this sequence
}

let rec buildRevReplacements (sequence: string list): RevMap =
  let matches =
    replacements
    |> Map.toSeq
    |> Seq.collect (fun (x, y) -> y |> List.map (fun z -> x, z))
    |> Seq.filter (fun (_, x) -> x.Length >= sequence.Length && (x |> List.take sequence.Length) = sequence)
    |> Seq.toList
  let exactMatches = matches |> List.filter (snd >> (=) sequence)
  let partialMatches = matches |> List.except exactMatches
  let nextElements = partialMatches |> List.map (fun (_, x) -> x |> List.skip sequence.Length |> List.head) |> Set
  let continuations = nextElements |> Seq.map (fun x -> x, buildRevReplacements (sequence @ [x])) |> Map
  { Elements = exactMatches |> List.map fst; Continuations = continuations }

let rootRevReplacements = buildRevReplacements []

// Get all molecules that could have produced the given module if one of its elements were replaced.
let rec findAllRevReplacements = function
  | [] -> Seq.empty
  | head :: tail ->
    let headReplacements =
      head :: tail |> findRevReplacements |> Seq.map (fun (len, elem) -> elem :: tail.[len-1..])
    let tailReplacements =
      tail |> findAllRevReplacements |> Seq.map (fun x -> head :: x)
    headReplacements |> Seq.append tailReplacements

and findRevReplacements molecule =
  let rec search map len molecule = seq {
    if not (molecule |> List.isEmpty) then
      match map.Continuations |> Map.tryFind (molecule |> List.head) with
      | None -> ()
      | Some map -> yield! search map (len + 1) (molecule |> List.tail)
    yield! map.Elements |> List.map (fun x -> len, x)
  }
  search rootRevReplacements 0 molecule

// Given a sequence, perform reverse replacements until you get down to a single element. Return all individual
// elements that can be reached, along with how many steps it takes to get there.
let drillDown sequence =
  let infinity = sequence |> List.length // each step decreases size by at least 1, so this is a reasonable upper-bound
  let mutable shortestPaths = Map.empty
  let rec search steps molecule =
    let prev = shortestPaths |> Map.tryFind molecule |> Option.defaultValue infinity
    if steps >= prev then
      () // we've already gotten here in a smaller (or equal) number of steps
    else
      shortestPaths <- shortestPaths |> Map.add molecule steps
      molecule |> findAllRevReplacements |> Seq.iter (search (steps + 1))
  search 0 sequence
  shortestPaths |> Map.filter (fun x _ -> x.Length = 1) |> Map.toSeq |> Seq.map (fun (x, y) -> x.[0], y) |> Map

// Break the full sequence into a number of sub-sequences ending in an outer-most "Ar". We ignore "Ar"s that are part
// of an inner reaction (think parentheses).
let segmentize sequence =
  let rec segmentize cur level sequence =
    match sequence with
    | [] -> if cur |> List.isEmpty then [] else [cur]
    | "Ar" :: tail when level = 1 -> ("Ar" :: cur) :: (tail |> segmentize [] 0)
    | "Ar" :: tail -> tail |> segmentize ("Ar" :: cur) (level - 1)
    | "Rn" :: tail -> tail |> segmentize ("Rn" :: cur) (level + 1)
    | x :: tail -> tail |> segmentize (x :: cur) level
  sequence |> segmentize [] 0 |> List.map List.rev

// Work through the sub-segments one at a time, carrying the individual source element forward.
let segments = initMolecule |> segmentize
let mutable map = segments |> List.head |> drillDown
for seg in segments |> List.tail do
  let infinity = System.Int32.MaxValue
  let mergeResult result elem steps =
    let cur = result |> Map.tryFind elem |> Option.defaultValue infinity
    result |> Map.add elem (min cur steps)
  let mergeMaps result map = map |> Map.fold mergeResult result
  map <-
    map
    |> Map.toSeq
    |> Seq.map (fun (e, l) -> e :: seg |> drillDown |> Map.map (fun _ v -> v + l))
    |> Seq.fold mergeMaps Map.empty

printfn "Part 2: %d" (map |> Map.find "e")

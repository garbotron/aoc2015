let neighbors (x, y) = [
  x - 1, y - 1
  x - 1, y
  x - 1, y + 1
  x, y - 1
  x, y + 1
  x + 1, y - 1
  x + 1, y
  x + 1, y + 1 ]

let nextState coord lights =
  let on x = lights |> Map.tryFind x |> Option.defaultValue false
  let neighborCount = coord |> neighbors |> List.map on |> List.filter id |> List.length
  match (on coord, neighborCount) with
  | true, 2 -> true
  | true, 3 -> true
  | true, _ -> false
  | false, 3 -> true
  | false, _ -> false

let nextLights lights = lights |> Map.map (fun x _ -> lights |> nextState x)
let stickCorners lights = [0, 0; 0, 99; 99, 0; 99, 99] |> List.fold (fun m x -> m |> Map.add x true) lights
let onCount lights = lights |> Map.toSeq |> Seq.filter snd |> Seq.length
let repeat fn n = List.init n (fun _ -> fn) |> List.reduce (>>)

let foldLine lights (y, str) =
  str |> Seq.indexed |> Seq.fold (fun lights (x, c) -> lights |> Map.add (x, y) (c = '#')) lights
let input = System.IO.File.ReadAllLines "day-18-input.txt" |> Seq.indexed |> Seq.fold foldLine Map.empty

printfn "Part 1: %d" (input |> repeat nextLights 100 |> onCount)
printfn "Part 2: %d" (input |> stickCorners |> repeat (nextLights >> stickCorners) 100 |> onCount)

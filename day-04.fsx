let md5 str =
  str
  |> Seq.map byte
  |> Seq.toArray
  |> System.Security.Cryptography.MD5.HashData
  |> Seq.map (sprintf "%02x")
  |> Seq.reduce (+)

let md5s key = Seq.initInfinite (fun i -> key + string i |> md5)
let startsWith (substr: string) (str: string) = str.StartsWith substr

printfn "Part 1: %d" (md5s "bgvyzdsv" |> Seq.indexed |> Seq.find (snd >> startsWith "00000") |> fst)
printfn "Part 2: %d" (md5s "bgvyzdsv" |> Seq.indexed |> Seq.find (snd >> startsWith "000000") |> fst)

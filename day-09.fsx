let rec permutations set =
  match set |> Seq.tryExactlyOne with
  | Some x -> seq {[x]}
  | None -> set |> Seq.collect (fun x -> permutations (set |> Set.remove x) |> Seq.map (fun y -> x :: y))

let foldInputLine dists (str: string) =
  let words = str.Split ' '
  let loc1 = words.[0]
  let loc2 = words.[2]
  let dist = words.[4] |> int
  dists |> Map.add (loc1, loc2) dist |> Map.add (loc2, loc1) dist

let dists = System.IO.File.ReadAllLines "day-09-input.txt" |> Seq.fold foldInputLine Map.empty
let locations = dists |> Map.toSeq |> Seq.map (fst >> fst) |> Set

let totalDistance permutation =
  permutation |> Seq.pairwise |> Seq.sumBy (fun (x, y) -> dists.[x, y])

printfn "Part 1: %d" (locations |> permutations |> Seq.map totalDistance |> Seq.min)
printfn "Part 2: %d" (locations |> permutations |> Seq.map totalDistance |> Seq.max)

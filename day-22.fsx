type State = {
  HP: int
  Mana: int
  Spent: int
  Armor: int
  BossHP: int
  BossDamage: int
  ShieldTimer: int
  PoisonTimer: int
  RechargeTimer: int
  ConstantDamage: int
}

type Spell = {
  Cost: int
  Action: State -> State
  Precondition: State -> bool
}

let spells = [
  { Cost = 53 // Magic Missile
    Action = fun s -> { s with BossHP = s.BossHP - 4 }
    Precondition = fun _ -> true }
  { Cost = 73 // Drain
    Action = fun s -> { s with BossHP = s.BossHP - 2; HP = s.HP + 2 }
    Precondition = fun _ -> true }
  { Cost = 113 // Shield
    Action = fun s -> { s with ShieldTimer = 6 }
    Precondition = fun s -> s.ShieldTimer = 0 }
  { Cost = 173 // Poison
    Action = fun s -> { s with PoisonTimer = 6 }
    Precondition = fun s -> s.PoisonTimer = 0 }
  { Cost = 229 // Recharge
    Action = fun s -> { s with RechargeTimer = 5 }
    Precondition = fun s -> s.RechargeTimer = 0 }
]

let shield state =
  match state.ShieldTimer with
  | 0 -> { state with Armor = 0 }
  | n -> { state with Armor = 7; ShieldTimer = n - 1 }

let poison state =
  match state.PoisonTimer with
  | 0 -> state
  | n -> { state with BossHP = state.BossHP - 3; PoisonTimer = n - 1 }

let recharge state =
  match state.RechargeTimer with
  | 0 -> state
  | n -> { state with Mana = state.Mana + 101; RechargeTimer = n - 1 }

let cast spell state =
  { state with Mana = state.Mana - spell.Cost; Spent = state.Spent + spell.Cost } |> spell.Action

let potentialPlayerTurns state =
  let state = { state with HP = state.HP - state.ConstantDamage }
  if state.HP <= 0 then
    Seq.empty // died to constant damage
  else
    let state = state |> shield |> poison |> recharge
    spells
      |> Seq.filter (fun x -> x.Cost <= state.Mana && x.Precondition state)
      |> Seq.map (fun x -> state |> cast x)

let bossTurn state =
  let state = state |> shield |> poison |> recharge
  if state.BossHP <= 0 then
    state
  else
    let dmg = max 1 (state.BossDamage - state.Armor)
    { state with HP = state.HP - dmg }

let rec minManaToWin maxTurns state =
  if state.BossHP <= 0 then
    Some state.Spent
  elif state.HP <= 0 || maxTurns = 0 then
    None
  else
    state
      |> potentialPlayerTurns
      |> Seq.map bossTurn
      |> Seq.choose (minManaToWin (maxTurns - 1))
      |> Seq.sort
      |> Seq.tryHead

let initialState constantDamage = {
  HP = 50
  Mana = 500
  Spent = 0
  Armor = 0
  BossHP = 55
  BossDamage = 8
  ShieldTimer = 0
  PoisonTimer = 0
  RechargeTimer = 0
  ConstantDamage = constantDamage
}

printfn "Part 1: %d" (initialState 0 |> minManaToWin 15 |> Option.get)
printfn "Part 2: %d" (initialState 1 |> minManaToWin 15 |> Option.get)

let input = System.IO.File.ReadAllText("day-01-input.txt").Trim()

printfn "Part 1: %d" (input |> Seq.sumBy (fun x -> if x = '(' then 1 else -1))
input
  |> Seq.scan (fun level x -> if x = '(' then level + 1 else level - 1) 0
  |> Seq.indexed
  |> Seq.find (fun (_, x) -> x < 0)
  |> fst
  |> printfn "Part 2: %d"

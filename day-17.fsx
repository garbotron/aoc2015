let input = System.IO.File.ReadAllLines "day-17-input.txt" |> Seq.map int |> Seq.toList

let rec findCombinations (weight: int) (containers: int list): (int list) seq =
  match containers with
  | _ when weight = 0 -> seq {[]}
  | _ when weight < 0 -> Seq.empty
  | head :: tail ->
    // It either uses the first container, or it doesn't.
    tail
    |> findCombinations (weight - head)
    |> Seq.map (fun x -> head :: x)
    |> Seq.append (tail |> findCombinations weight)
  | _ -> Seq.empty

let combinations = input |> findCombinations 150 |> Seq.toList
let minContainerCount = combinations |> List.map List.length |> List.min

printfn "Part 1: %d" combinations.Length
printfn "Part 2: %d" (combinations |> List.filter (fun x -> x.Length = minContainerCount) |> List.length)

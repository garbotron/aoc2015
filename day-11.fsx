open System.Text.RegularExpressions

let next ch = ch |> int |> (+) 1 |> char

let rec increment place (str: string) =
  match str.[place] with
  | 'z' -> str.[..place-1] + "a" + str.[place+1..] |> increment (place - 1)
  | c -> str.[..place-1] + (c |> next |> string) + str.[place+1..]

let legal (str: string) =
  let isStraight (win: char list) = win.[1] = next win.[0] && win.[2] = next win.[1]
  let hasStraight = str |> Seq.toList |> List.windowed 3 |> List.exists isStraight
  let hasIllegalChars = str.IndexOfAny [| 'i'; 'o'; 'l' |] > 0
  let has2pairs = Regex.Matches(str, @"(\w)\1").Count >= 2
  hasStraight && not hasIllegalChars && has2pairs

let rec incrementLegal (str: string) =
  let next = str |> increment 7
  if legal next then next else incrementLegal next

printfn "Part 1: %s" ("cqjxjnds" |> incrementLegal)
printfn "Part 2: %s" ("cqjxjnds" |> incrementLegal |> incrementLegal)

open System.Text.RegularExpressions

let hexint s = System.Int32.Parse(s, System.Globalization.NumberStyles.HexNumber)

let decode (str: string) =
  let str = str.Substring(1, str.Length - 2).Replace(@"\\", @"\").Replace(@"\""", @"""")
  Regex.Replace(str, @"\\x([0-9a-f]{2})", fun m -> m.Groups.[1].Value |> hexint |> char |> string)

let encode (str: string) =
  sprintf @"""%s""" (str.Replace(@"\", @"\\").Replace(@"""", @"\"""))

let input = System.IO.File.ReadAllLines "day-08-input.txt"
printfn "Part 1: %d" (input |> Seq.sumBy (fun s -> s.Length - (decode s).Length))
printfn "Part 2: %d" (input |> Seq.sumBy (fun s -> (encode s).Length - s.Length))

open System.Text.RegularExpressions

type Reindeer = { Name: string; Speed: int; FlyPeriod: int; RestPeriod: int }

let parseLine (str: string) =
  let m = Regex.Match(str, @"(\w+) can fly (\d+) km/s for (\d+) seconds, but then must rest for (\d+) seconds")
  { Name = m.Groups.[1].Value
    Speed = m.Groups.[2].Value |> int
    FlyPeriod = m.Groups.[3].Value |> int
    RestPeriod = m.Groups.[4].Value |> int }

let input = System.IO.File.ReadAllLines "day-14-input.txt" |> Seq.map parseLine |> Seq.toList

let position time reindeer =
  let completeCycles = time / (reindeer.FlyPeriod + reindeer.RestPeriod)
  let intoLastCycle = time % (reindeer.FlyPeriod + reindeer.RestPeriod)
  (completeCycles * (reindeer.Speed * reindeer.FlyPeriod)) + ((min intoLastCycle reindeer.FlyPeriod) * reindeer.Speed)

let rec cumulativeScore time reindeer =
  match time with
  | 0 -> 0
  | n ->
    let pos = reindeer |> position n
    let score = if input |> List.map (position n) |> List.forall (fun x -> pos >= x) then 1 else 0
    score + (reindeer |> cumulativeScore (n - 1))

printfn "Part 1: %d" (input |> Seq.map (position 2503) |> Seq.max)
printfn "Part 2: %d" (input |> Seq.map (cumulativeScore 2503) |> Seq.max)

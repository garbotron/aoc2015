type Instruction =
  | Hlf of char
  | Tpl of char
  | Inc of char
  | Jmp of int
  | Jie of char * int
  | Jio of char * int

type Cpu = {
  Registers: Map<char, int>
  Program: Map<int, Instruction>
  PC: int
}

let getReg reg cpu = cpu.Registers |> Map.tryFind reg |> Option.defaultValue 0
let setReg reg v cpu = { cpu with Registers = cpu.Registers |> Map.add reg v }

let rec exec cpu =
  let step n cpu = exec { cpu with PC = cpu.PC + n }
  match cpu.Program |> Map.tryFind cpu.PC with
  | None -> cpu // out of bounds
  | Some (Hlf reg) -> cpu |> setReg reg (getReg reg cpu / 2) |> step 1
  | Some (Tpl reg) -> cpu |> setReg reg (getReg reg cpu * 3) |> step 1
  | Some (Inc reg) -> cpu |> setReg reg (getReg reg cpu + 1) |> step 1
  | Some (Jmp n) -> cpu |> step n
  | Some (Jie (reg, n)) -> cpu |> step (if getReg reg cpu % 2 = 0 then n else 1)
  | Some (Jio (reg, n)) -> cpu |> step (if getReg reg cpu = 1 then n else 1)

let parseInstruction (str: string) =
  let words = str.Split " "
  match words.[0] with
  | "hlf" -> Hlf words.[1].[0]
  | "tpl" -> Tpl words.[1].[0]
  | "inc" -> Inc words.[1].[0]
  | "jmp" -> Jmp (words.[1] |> int)
  | "jie" -> Jie (words.[1].[0], words.[2] |> int)
  | "jio" -> Jio (words.[1].[0], words.[2] |> int)
  | _ -> failwith "bad instruction"

let initCpu = {
  Registers = Map.empty
  Program = System.IO.File.ReadAllLines "day-23-input.txt" |> Seq.map parseInstruction |> Seq.indexed |> Map
  PC = 0
}

printfn "Part 1: %d" (initCpu |> exec |> getReg 'b')
printfn "Part 2: %d" (initCpu |> setReg 'a' 1 |> exec |> getReg 'b')

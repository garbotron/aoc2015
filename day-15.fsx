open System.Text.RegularExpressions

type Ingredient = { Name: string; Capacity: int; Durability: int; Flavor: int; Texture: int; Calories: int }

let parseLine (str: string) =
  let regex = @"(\w+): capacity (-?\d+), durability (-?\d+), flavor (-?\d+), texture (-?\d+), calories (-?\d+)"
  let m = Regex.Match(str, regex)
  { Name = m.Groups.[1].Value
    Capacity = m.Groups.[2].Value |> int
    Durability = m.Groups.[3].Value |> int
    Flavor = m.Groups.[4].Value |> int
    Texture = m.Groups.[5].Value |> int
    Calories = m.Groups.[6].Value |> int }

let ingredients = System.IO.File.ReadAllLines "day-15-input.txt" |> Seq.map parseLine |> Seq.toList
let ingredientNames = ingredients |> List.map (fun x -> x.Name)
let ingredientMap = Seq.zip ingredientNames ingredients |> Map

let repeat fn n = List.init n (fun _ -> fn) |> List.reduce (>>)

let score recipe =
  let a = recipe |> Map.toSeq |> Seq.sumBy (fun (name, count) -> ingredientMap.[name].Capacity * count)
  let b = recipe |> Map.toSeq |> Seq.sumBy (fun (name, count) -> ingredientMap.[name].Durability * count)
  let c = recipe |> Map.toSeq |> Seq.sumBy (fun (name, count) -> ingredientMap.[name].Flavor * count)
  let d = recipe |> Map.toSeq |> Seq.sumBy (fun (name, count) -> ingredientMap.[name].Texture * count)
  (max a 0) * (max b 0) * (max c 0) * (max d 0)

let addTeaspoon recipe =
  // Add whichever ingredient would result in the highest new score.
  let addIngredient ingr = recipe |> Map.add ingr ((recipe |> Map.find ingr) + 1)
  addIngredient (ingredientNames |> Seq.maxBy (addIngredient >> score))

let calorieCount recipe =
  recipe |> Map.toSeq |> Seq.sumBy (fun (i, c) -> ingredientMap.[i].Calories * c)

// Start with 1 teaspoon of everything.
let oneOfEach = ingredientMap |> Map.map (fun _ _ -> 1)
let bestRecipe = oneOfEach |> repeat addTeaspoon (100 - ingredients.Length)
printfn "Part 1: %d" (bestRecipe |> score)

// For part 2, I used Wolfram Alpha to calculate "{5a + 8b + 6c + d = 500, a + b + c + d = 100}" and got:
//   b = 3 a + 5 n, c = -5 a - 7 n + 80, d = a + 2 n + 20, n element Z
// So, given a in [1..100] and n in [all integers], the above formulas give us b, c and d.
// We'll give n the range [-1000..1000] which is massive overkill, but that's fine for our purposes.
let p2variations = seq {
  for a in [1..100] do
    for n in [-1000..1000] do
      let b = 3*a + 5*n
      let c = -5*a - 7*n + 80
      let d = a + 2*n + 20
      if [b; c; d] |> List.forall (fun x -> x >= 1 && x <= 100) then
        yield [a; b; c; d] }

let p2recipes = p2variations |> Seq.map (fun x -> Seq.zip ingredientNames x |> Map)
printfn "Part 2: %d" (p2recipes |> Seq.map score |> Seq.max)

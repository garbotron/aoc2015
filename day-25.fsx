let codes startValue = seq {
  let mutable code = startValue
  for cycle in Seq.initInfinite ((+) 1) do
    for i in { 1..cycle } do
      yield (cycle - i + 1, i, code)
      code <- (code * 252533L) % 33554393L
}

codes 20151125L
  |> Seq.choose (fun (row, col, code) -> if row = 3010 && col = 3019 then Some code else None)
  |> Seq.head
  |> printfn "Part 1: %d"

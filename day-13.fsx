open System.Text.RegularExpressions

let rec permutations set =
  match set |> Seq.tryExactlyOne with
  | Some x -> seq {[x]}
  | None -> set |> Seq.collect (fun x -> permutations (set |> Set.remove x) |> Seq.map (fun y -> x :: y))

let foldInputLine dists (str: string) =
  let m = Regex.Match(str, @"(\w+) would (gain|lose) (\d+) happiness units by sitting next to (\w+).")
  let a = m.Groups.[1].Value
  let b = m.Groups.[4].Value
  let magnitude = m.Groups.[3].Value |> int
  let delta = if m.Groups.[2].Value = "lose" then -magnitude else magnitude
  dists |> Map.add (a, b) delta

let happiness = System.IO.File.ReadAllLines "day-13-input.txt" |> Seq.fold foldInputLine Map.empty
let people = happiness |> Map.toSeq |> Seq.map (fst >> fst) |> Set

let happinessForPair (x, y) =
  if x = "me" || y = "me" then 0 else happiness.[x, y] + happiness.[y, x]

let totalHappiness permutation =
  Seq.concat [permutation; [List.head permutation]] |> Seq.pairwise |> Seq.sumBy happinessForPair

printfn "Part 1: %d" (people |> permutations |> Seq.map totalHappiness |> Seq.max)
printfn "Part 2: %d" (people |> Set.add "me" |> permutations |> Seq.map totalHappiness |> Seq.max)

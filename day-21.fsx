type Result = PlayerWin | BossWin
type Combatant = { HP: int; Damage: int; Armor: int }
type Item = { Cost: int; DamageMod: int; ArmorMod: int }

let boss = { HP = 104; Damage = 8; Armor = 1 }
let nakedPlayer = { HP = 100; Damage = 0; Armor = 0 }
let weapons = [
  { Cost = 8 ; DamageMod = 4; ArmorMod = 0 } // Dagger
  { Cost = 10; DamageMod = 5; ArmorMod = 0 } // Shortsword
  { Cost = 25; DamageMod = 6; ArmorMod = 0 } // Warhammer
  { Cost = 40; DamageMod = 7; ArmorMod = 0 } // Longsword
  { Cost = 74; DamageMod = 8; ArmorMod = 0 } // Greataxe
]
let armor = [
  { Cost = 0  ; DamageMod = 0; ArmorMod = 0 } // Unarmored
  { Cost = 13 ; DamageMod = 0; ArmorMod = 1 } // Leather
  { Cost = 31 ; DamageMod = 0; ArmorMod = 2 } // Chainmail
  { Cost = 53 ; DamageMod = 0; ArmorMod = 3 } // Splintmail
  { Cost = 75 ; DamageMod = 0; ArmorMod = 4 } // Bandedmail
  { Cost = 102; DamageMod = 0; ArmorMod = 5 } // Platemail
]
let rings = [
  { Cost = 25 ; DamageMod = 1; ArmorMod = 0 } // Damage +1
  { Cost = 50 ; DamageMod = 2; ArmorMod = 0 } // Damage +2
  { Cost = 100; DamageMod = 3; ArmorMod = 0 } // Damage +3
  { Cost = 20 ; DamageMod = 0; ArmorMod = 1 } // Defense +1
  { Cost = 40 ; DamageMod = 0; ArmorMod = 2 } // Defense +2
  { Cost = 80 ; DamageMod = 0; ArmorMod = 3 } // Defense +3
]

let damage attacker defender =
  max 1 (attacker.Damage - defender.Armor)

let rec fight boss player =
  let boss = { boss with HP = boss.HP - damage player boss }
  if boss.HP <= 0 then
    PlayerWin
  else
    let player = { player with HP = player.HP - damage boss player }
    if player.HP <= 0 then BossWin else fight boss player

let outfits = seq {
  for weapon in weapons do
    for armor in armor do
      yield [weapon; armor] // no rings
      for (i, r1) in rings |> Seq.indexed do
        yield [weapon; armor; r1] // one ring
        for r2 in rings |> Seq.skip (i + 1) do
          yield [weapon; armor; r1; r2] // two rings
}

let equip outfit player =
  { player with
      Damage = outfit |> List.sumBy (fun x -> x.DamageMod)
      Armor = outfit |> List.sumBy (fun x -> x.ArmorMod) }

let isWinningOutfit outfit = (nakedPlayer |> equip outfit |> fight boss) = PlayerWin
let totalCost outfit = outfit |> List.sumBy (fun x -> x.Cost)

printfn "Part 1: %d" (outfits |> Seq.filter isWinningOutfit |> Seq.map totalCost |> Seq.min)
printfn "Part 2: %d" (outfits |> Seq.filter (not << isWinningOutfit) |> Seq.map totalCost |> Seq.max)

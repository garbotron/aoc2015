let input =
  System.IO.File.ReadAllLines "day-24-input.txt"
  |> Array.toList
  |> List.map int
  |> List.sort

let div3 = List.sum input / 3
let div4 = List.sum input / 4

let rec subgroups targetWeight remainingCount packages =
  if targetWeight = 0 && remainingCount = 0 then
    seq {[]}
  elif targetWeight < 0 || remainingCount = 0 || (packages |> List.length) < remainingCount then
    Seq.empty
  else
    // Now, either the group includes the head of the list, or it doesn't.
    let head = packages |> List.head
    let tail = packages |> List.tail
    let withHead = tail |> subgroups (targetWeight - head) (remainingCount - 1) |> Seq.map (fun x -> head :: x)
    let withoutHead = tail |> subgroups targetWeight remainingCount
    withHead |> Seq.append withoutHead

let smallestSubgroups targetWeight packages =
  Seq.initInfinite id
  |> Seq.map (fun x -> packages |> subgroups targetWeight x)
  |> Seq.find (not << Seq.isEmpty)

let quantumEntaglement = List.map int64 >> List.reduce (*)

printfn "Part 1: %d" (input |> smallestSubgroups div3 |> Seq.map quantumEntaglement |> Seq.min)
printfn "Part 2: %d" (input |> smallestSubgroups div4 |> Seq.map quantumEntaglement |> Seq.min)

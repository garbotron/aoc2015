let tickerTape = Map <| [
  "children", 3
  "cats", 7
  "samoyeds", 2
  "pomeranians", 3
  "akitas", 0
  "vizslas", 0
  "goldfish", 5
  "trees", 3
  "cars", 2
  "perfumes", 1]

let parseSue (str: string) =
  let split = str.[str.IndexOf(":")+2..].Split(", ")
  split |> Seq.map (fun x -> x.Split ": " |> fun x -> x.[0], int x.[1]) |> Map

let input = System.IO.File.ReadAllLines "day-16-input.txt" |> Seq.map parseSue |> Seq.toList

let sueMatches sue =
  sue |> Map.toSeq |> Seq.forall (fun (x, y) -> tickerTape.[x] = y)

let sueMatchesRange sue =
  let matches = function
    | "cats", x -> x > tickerTape.["cats"]
    | "trees", x -> x > tickerTape.["trees"]
    | "pomeranians", x -> x < tickerTape.["pomeranians"]
    | "goldfish", x -> x < tickerTape.["goldfish"]
    | key, x -> x = tickerTape.[key]
  sue |> Map.toSeq |> Seq.forall matches

printfn "Part 1: %d" (input |> Seq.indexed |> Seq.find (snd >> sueMatches) |> fst |> (+) 1)
printfn "Part 2: %d" (input |> Seq.indexed |> Seq.find (snd >> sueMatchesRange) |> fst |> (+) 1)

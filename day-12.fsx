#r "nuget: Newtonsoft.Json"
open Newtonsoft.Json.Linq

let rec allNumbers (excludeObj: JObject -> bool) (token: JToken) = seq {
  match token with
  | :? JValue as v when v.Type = JTokenType.Integer -> yield v.Value :?> int64
  | :? JObject as o when excludeObj o -> ()
  | :? JContainer as c -> yield! (c |> Seq.collect (allNumbers excludeObj))
  | _ -> ()
}

let isRed (obj: JObject) =
  let isRedProperty (prop: JProperty) =
    prop.Value.Type = JTokenType.String && prop.Value.Value<string>() = "red"
  obj.Properties() |> Seq.exists isRedProperty

let input = System.IO.File.ReadAllText "day-12-input.txt" |> JObject.Parse
printfn "Part 1: %d" (input |> allNumbers (fun _ -> false) |> Seq.sum)
printfn "Part 2: %d" (input |> allNumbers isRed |> Seq.sum)

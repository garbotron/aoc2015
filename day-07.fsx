type Source = Number of uint16 | Wire of string | Invalid
type InputType = Direct | Not | And | Or | LShift | RShift
type Input = { Type: InputType; A: Source; B: Source }

let rec fillValueMap values input =
  let isResolved = function
    | Wire x -> values |> Map.containsKey x
    | _ -> true

  let resolveSource = function
    | Number x -> x
    | Wire x -> values |> Map.find x
    | Invalid -> failwith "bad source"

  let resolveInput x =
    match x.Type with
    | Direct -> resolveSource x.A
    | Not -> ~~~(resolveSource x.A)
    | And -> (resolveSource x.A) &&& (resolveSource x.B)
    | Or -> (resolveSource x.A) ||| (resolveSource x.B)
    | LShift -> (resolveSource x.A) <<< (resolveSource x.B |> int)
    | RShift -> (resolveSource x.A) >>> (resolveSource x.B |> int)

  // Find all wires whose inputs we have resolved but don't have a value yet and calculate their values.
  // If there are none left, we're done, otherwise recurse.
  let unresolved =
    input
    |> Map.filter (fun w i -> not (values |> Map.containsKey w) && isResolved i.A && isResolved i.B)
  if unresolved.IsEmpty then
    values
  else
    let values = unresolved |> Map.toSeq |> Seq.fold (fun m (w, i) -> m |> Map.add w (resolveInput i)) values
    input |> fillValueMap values

let parseSource (str: string) =
  if System.Char.IsDigit str.[0] then (str |> uint16 |> Number) else (str |> Wire)

let foldInputLine wires (str: string) =
  let split = str.Split " -> "
  let dest = split.[1]
  let split = split.[0].Split ' '
  let feed =
    if split.Length = 1 then
      { Type = Direct; A = parseSource split.[0]; B = Invalid }
    elif split.[0] = "NOT" then
      { Type = Not; A = parseSource split.[1]; B = Invalid }
    else
      match split.[1] with
      | "AND" ->    { Type = And;    A = parseSource split.[0]; B = parseSource split.[2] }
      | "OR" ->     { Type = Or;     A = parseSource split.[0]; B = parseSource split.[2] }
      | "LSHIFT" -> { Type = LShift; A = parseSource split.[0]; B = parseSource split.[2] }
      | "RSHIFT" -> { Type = RShift; A = parseSource split.[0]; B = parseSource split.[2] }
      | _ -> failwith "bad input"
  wires |> Map.add dest feed

let input = System.IO.File.ReadAllLines "day-07-input.txt" |> Seq.fold foldInputLine Map.empty

let values = input |> fillValueMap Map.empty
printfn "Part 1: %d" values.["a"]

let modInput = input |> Map.add "b" { Type = Direct; A = Number values.["a"]; B = Invalid }
let modValues = modInput |> fillValueMap Map.empty
printfn "Part 2: %d" modValues.["a"]
